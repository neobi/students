$(document).ready( function () {
    setList();
    initializeTable();
});

function setList() {
    if (localStorage.getItem("student_list")) {
        var mydata = JSON.parse(localStorage.getItem("student_list"));
        let html = ``;
        mydata.forEach(function(elem, index) {
            if (elem.gender == 'male') {
                elem.gender = "Мужской";
            } else {
                elem.gender = "Женский";
            }
            let result = createTable(elem.id, elem.name, elem.surname, elem.gender, elem.address, elem.mail);
            html += result;
        });
        $('.students__table-body').html(html);
    }
}
function createTable(
    id,
    name = '',
    surname = '',
    gender = '',
    address = '',
    mail = ''
) {
    let html = `
                <tr>
                    <th class="student__info" scope="row">${id}</th>
                    <td class="student__info">${name}</td>
                    <td class="student__info">${surname}</td>
                    <td class="student__info">${gender}</td>
                    <td class="student__info">${mail}</td>
                    <td class="student__info">${address}</td>
                    <td class="student__info-all"><i onclick="detail(${id})" class="fa fa-info-circle"></i></td>
                    <td class="student__info-edit"><i onclick="edit(${id})" class="fa fa-edit"> </i></td>
                    <td class="student__info-delete"><i onclick="remove(${id})" id="remove_${id}" class="fa fa-trash-o"></i></td>
                  </tr>`
    return html;    
}

function initializeTable() {
    $('.students__table').DataTable({
        "language" : {
            "url" : "dataTableRu.json"
        },
        "paging": true,
  
        "pageLength": 4,
  
        "lengthChange": false,
  
        "searching": true,
  
        "ordering": false,
  
        "info": true,
  
        "autoWidth": false
  
      });
}

function detail(id) {
    localStorage.setItem("student_id", JSON.stringify(id))
}
function edit(id) {
    localStorage.setItem("student_id", JSON.stringify(id))
}
function remove(id) {
    let table = $('.students__table').DataTable();;
    if (localStorage.getItem("student_list")) {
        var mydata = JSON.parse(localStorage.getItem("student_list"));
        mydata.forEach(function(elem, index) {
            if (elem.id == id) {
                mydata.splice(index, 1);
            } 
        });
        localStorage.setItem("student_list", JSON.stringify(mydata))
    }
    table
    .row( $(`#remove_${id}`).parents('tr') )
    .remove()
    .draw();
}