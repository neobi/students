$(document).ready(function() {
    validate();
})
function addStudent() {
    $("#addStudentForm").submit(function(e) {
        e.preventDefault();
    });
    var array = [];
    var name = $('#name').val();
    var surname = $('#surname').val();
    var bdate = $('#bdate').val();
    var gender = $('#gender').find(":selected").text();;
    var address = $('#address').val();
    var city = $('#city').val();
    var country = $('#country').find(":selected").text();;
    var phone = $('#phone').val();
    var mail = $('#mail').val();
    var obj = {
        id: 1,
        name: name,
        surname: surname,
        bdate: bdate,
        gender: gender,
        address: address,
        city: city,
        country: country,
        phone: phone,
        mail: mail,
    }

    var found = false;
    let studentList = localStorage.getItem("student_list");
    if (studentList && JSON.parse(localStorage.getItem("student_list")).length > 0) {
        var mydata = JSON.parse(localStorage.getItem("student_list"));
        mydata.forEach(function(elem, index) {
            if (elem && elem.mail === mail) {
                found = true;
            }
        });
        if (!found) {
            obj.id = mydata[mydata.length-1].id + 1;
            mydata.push(obj)
            localStorage.setItem("student_list", JSON.stringify(mydata))
            window.location.replace("main.html");
        }
    } else {
        array.push(obj);
        localStorage.setItem("student_list", JSON.stringify(array))
        window.location.replace("main.html");
    }
}


function validate() {
    jQuery.validator.setDefaults({
        debug: true,
        success: "valid"
    });

    $('#addStudentForm').validate({
        lang: 'ru',
        rules: {
            name: {
                required: true,
                maxlength: 80,
                minlength: 3
            },
            surname: {
                required: true,
                maxlength: 80,
                minlength: 3
            },
            bdate: {
                required: true
            },
            gender: {
                required: true
            },
            address: {
                required: true,
                maxlength: 150,
                minlength: 3
            },
            city: {
                required: true
            },
            country: {
                required: true
            },
            cell_phone: {
                required: false,
                number: true
            },
            mail: {
                required: true,
                email: true
            }
        },

        messages: {
            delivery:{
                required: "Пожалуйста выберите тип доставки"
            },
            name:  {
                required: "Пожалуйста введите имя",
            },
            surname:  {
                required: "Пожалуйста введите фамилию",
            },
            address:  {
                required: "Пожалуйста введите адрес",
            },
            bdate:  {
                required: "Пожалуйста выберите дату рождения",
            },
            gender:  {
                required: "Пожалуйста выберите пол",
            },
            city:  {
                required: "Пожалуйста введите город",
            },
            country:  {
                required: "Пожалуйста выберите страну",
            },
            mail:  {
                required: "Пожалуйста введите электронный адрес",
            },
        }
    });
}
